var webdb = {};
webdb.db = null;

webdb.open = function(){
	var dbSize = 10 * 1024 * 1024;
	webdb.db = openDatabase("energyapp", "1.0", "Energy consumption of the user", dbSize);
}

webdb.createTables = function(){
	webdb.db.transaction(function(tx){
		tx.executeSql("CREATE TABLE IF NOT EXISTS meters(id INTEGER PRIMARY KEY, date VARCHAR(10) UNIQUE, electricity INTEGER, gas INTEGER)", []);
		tx.executeSql("CREATE TABLE IF NOT EXISTS settings(id INTEGER PRIMARY KEY, key VARCHAR(255) UNIQUE, value VARCHAR(255))", []);
	});
}

webdb.dropTable = function(){
	webdb.db.transaction(function(tx){
		tx.executeSql("DROP TABLE meters", []);
	});
}

webdb.insertMeterData = function(electricity, gas, date, onSuccess, onError){
	if (!date){
		return //var date = moment().format("YYYY-MM-DD");
	}
	webdb.db.transaction(function(tx){
		tx.executeSql("REPLACE INTO meters(date, electricity, gas) VALUES (?, ?, ?)", [date, electricity, gas], onSuccess, onError);	
	});
}

webdb.onSuccess = function(tx, r){
	console.log("success");
}

webdb.onError = function(tx, e){
	console.log(e);
}

webdb.getMeterData = function(date, render) {
	webdb.db.transaction(function(tx) {
    	tx.executeSql("SELECT * FROM meters", [], function(tx, rs){ 
    		var data = [];
			for (var i=0; i < rs.rows.length; i++) {
				data.push(rs.rows.item(i));
	  		}
	  		render(data);
		});
	});
}

webdb.setSetting = function(key, value, onSuccess, onError){
	webdb.db.transaction(function(tx){
		tx.executeSql("REPLACE INTO settings(key, value) VALUES (?, ?)", [key, JSON.stringify(value)], function(){
			webdb.getSettings();
			if (onSuccess) { onSuccess(); }
		}, onError);	
		
	});
}

webdb.getSettings = function(key){
	webdb.db.transaction(function(tx) {
    	tx.executeSql("SELECT * FROM settings", [], function(tx, rs){ 
    		var data = {}
			for (var i=0; i < rs.rows.length; i++) {
				var key = rs.rows.item(i).key;
				var value = rs.rows.item(i).value;
				data[key] = JSON.parse(value);
	  		}
	  		webdb.settings = data;
		});
	});
}

webdb.getSetting = function(key){
	return webdb.settings[key];
}

webdb.open();
//webdb.dropTable();
webdb.createTables();
webdb.getSettings();
