// Setup translations
var LANG = "nl";

$.ajax({
	url: "locale/"+LANG+".js",
	dataType: "script",
	async: false
});

function _(string){
	if (string in translations){
		return translations[string];
	} else {
		return string;
	}
	
}

function translateAll(prefix){
	$(prefix+" .translate").each(function(){
		var $this = $(this);
		var tagName = $this.prop("tagName");
		if (tagName == "INPUT"){
			$this.attr("placeholder", _($this.attr("placeholder")));
		} else {
			$this.html(_($this.html()));
		}
		
	})
}
