from django.contrib.gis import admin
from weather.models import WeatherStation, WeatherCondition

admin.site.register(WeatherStation, admin.OSMGeoAdmin)
admin.site.register(WeatherCondition)