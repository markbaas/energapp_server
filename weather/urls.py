from django.contrib import admin
from django.conf.urls import patterns, url
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^index/$', 'weather.views.index'),
    url(r'^conditions/wuid/(?P<wuid>\w+)/(?P<begin>[\w-]+)/$', 'weather.views.get_conditions_by_weather_station'),
    url(r'^conditions/wuid/(?P<wuid>\w+)/(?P<begin>[\w-]+)/(?P<end>[\w-]+)/$', 'weather.views.get_conditions_by_weather_station'),
    url(r'^conditions/result/(\d+)/$', 'weather.views.get_conditions_result'),
    url(r'^station/coords/([\d\,\.-]+)/$', 'weather.views.get_closest_weather_station'),
    url(r'^stations/coords/([\d\,\.-]+)/distance/([\d-]+)/$', 'weather.views.get_all_nearby_stations')
)