# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django_countries import CountryField
from django.utils.translation import ugettext as _
import json
from energapp.settings import WU_API_KEY
import urllib2
from django.contrib.gis.geos.point import Point
import datetime
import time
import StringIO


WU_MIN_LIMIT_EXCEEDED = 1001
WU_DAY_LIMIT_EXCEEDED = 1002

def do_query(url):
    return WundergroundQuery.objects.get_or_create(date=datetime.date.today())[0].do_query(url)

class WuLimitExceeded(Exception):
    def __init__(self, value, eta):
        self.t = value
        self.eta = eta
    def __str__(self):
        return "Wunderground limit exceeded!"

class WeatherStation(models.Model):
    wuid = models.CharField(max_length=255, unique=True)
    coordinates = models.PointField(null=True, blank=True, geography=True)
    wuresponse = models.TextField(null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    
    objects = models.GeoManager()
    
    def __unicode__(self):
        if self.city and self.country:
            return u"{0}, {1} ({2})".format(self.city, self.country, self.wuid)
        else:
            if self.wuid.startswith("pws"):
                return u"PWS ({0})".format(self.wuid)
            else:
                return u"Airport ({0})".format(self.wuid)
            
    def to_dict(self):
        return {"name": self.__unicode__(),
                           "wuid": self.wuid, 
                          "coordinates": "{0},{1}".format(self.coordinates.get_x(), self.coordinates.get_y()),
                          "city": self.city,
                          "country": self.country.code
                        }
            
    def get_station_data(self):
        if not self.wuresponse:
            # If no data get it
            wuurl = "http://api.wunderground.com/api/{0}/geolookup/q/{1}.json".format(WU_API_KEY, self.wuid)
            wuresp = do_query(wuurl)
            self.wuresponse = wuresp.read()
           
            # Save avoiding getting in conflict with overriden function
            super(WeatherStation, self).save()
            
        return json.loads(self.wuresponse)
    
    def get_condition(self, date):        
        try: 
            weather_condition = WeatherCondition.objects.get(date=date, weather_station=self)
        except WeatherCondition.DoesNotExist, err:
            weather_condition = WeatherCondition(date=date, weather_station=self)
            weather_condition.save()
        return weather_condition
            
            
    def save(self):
        """
        Override save function to check for incomplete data
        """
        
        # Check for incomplete data about the weather station
        if not self.wuresponse:
            obj = self.get_station_data()
            self.city = obj["location"]["city"]
            self.country = obj["location"]["country"]
            self.coordinates = Point(float(obj["location"]["lon"]), float(obj["location"]["lat"]))
        
        # Call main save function
        super(WeatherStation, self).save()
    
class WeatherCondition(models.Model):
    date = models.DateField()
    weather_station = models.ForeignKey('WeatherStation')
    temp_min = models.IntegerField(null=True, blank=True)
    temp_max = models.IntegerField(null=True, blank=True)
    temp = models.IntegerField(null=True, blank=True)
    wind_direction = models.IntegerField(null=True, blank=True)
    wind_speed = models.IntegerField(null=True, blank=True)
    wuresponse = models.TextField(null=True, blank=True)
    
    objects = models.GeoManager()
    
    def __unicode__(self):
        return u"Weather at {0} ({4}): temp: {1}⁰ wind: {2} km/h {3}".format(self.weather_station.city, self.temp, self.wind_speed, self.wind_direction, self.date)
    
    def to_dict(self):
        return {"date": self.date.strftime("%Y-%m-%d"),
                "weather_station": self.weather_station.wuid,
                "temp_min": self.temp_min,
                "temp_max": self.temp_max,
                "temp": self.temp,
                "wind_direction": self.wind_direction,
                "wind_speed": self.wind_speed
                }
    
    def get_condition_data(self):
        if not self.wuresponse:
            # get feed for the whole year
            wuurl = "http://api.wunderground.com/api/{0}/history_{1}/q/{2}.json".format(WU_API_KEY, self.date.strftime("%Y%m%d"), self.weather_station.wuid)
            wuresp = do_query(wuurl)
            self.wuresponse = wuresp.read()
            
            # Save avoiding getting in conflict with overriden function
            super(WeatherCondition, self).save()
        
        return json.loads(self.wuresponse)

    def save(self):
        """
        Override save function to check for incomplete data
        """
        
        # Check for incomplete data about the weather station
        if not self.wuresponse:
            obj = self.get_condition_data()

            try:
                self.temp_min = obj["history"]["dailysummary"][0]["mintempm"]
                self.temp_max = obj["history"]["dailysummary"][0]["maxtempm"]
                self.temp = obj["history"]["dailysummary"][0]["meantempm"]
                self.wind_direction = obj["history"]["dailysummary"][0]["meanwdird"]
                self.wind_speed = obj["history"]["dailysummary"][0]["meanwindspdm"]
            except KeyError, err:
                self.delete()
                return
        
        # Call main save function
        super(WeatherCondition, self).save()
        
class WeatherRequest(models.Model):
    """
    Getting the data done takes a while, so this happens internally and the result is stored in this object.
    """
    date = models.DateTimeField(auto_now_add=True)
    result = models.TextField(blank=True, null=True)
    progress = models.FloatField(blank=True, null=True)
    sleep = models.IntegerField(blank=True, null=True)
    
class WundergroundQuery(models.Model):
    queries = models.IntegerField(null=True) # Number of queries
    date = models.DateField(auto_now_add=True)
    query_time = models.FloatField(null=True, blank=True)
    query_minute_count = models.FloatField(null=True, blank=True)
    limit = models.IntegerField(null=True, blank=True)
    
    def do_query(self, url):
        if not self.limit:
            self.limit = 10
        
        if not self.query_time or (time.time() - self.query_time > 60):
            self.query_time = time.time()
            self.query_minute_count = 0
                
        if (time.time() - self.query_time) < 60 and self.query_minute_count >= self.limit:
            diff = 60 - int(time.time() - self.query_time)
#            print self.query_time, time.time()
#            print "Query limit exceeded waiting {0} seconds...".format(diff)
           
            if (time.time() - self.query_time > 60):
                self.query_time = time.time()
                self.query_minute_count = 0        
            else:
                raise WuLimitExceeded(WU_MIN_LIMIT_EXCEEDED, diff)
                
        
        if not self.queries:
            self.queries = 1
        else:
            self.queries += 1
        
        data = urllib2.urlopen(url)
        #data = StringIO.StringIO("{}")
        
        self.query_minute_count += 1
        self.save()
        
        return data
        