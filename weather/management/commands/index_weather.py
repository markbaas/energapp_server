from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from weather.models import WeatherStation
from progressbar.widgets import Bar, Percentage, RotatingMarker, ETA
from progressbar.progressbar import ProgressBar
import time

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--reindex', '-r',
            action='store',
            dest='reindex',
            default=None,
            help='Forces a reindex for a comma separated list of wuids or \'all\''),
        )
    help = 'Loads missing data and (historical) weather conditions'

    def handle(self, *args, **options):
        # Handle options and get correct list of weather stations
        reindex = options["reindex"]
        if reindex:
            if reindex == "all":
                weather_stations = WeatherStation.objects.all()
            else:
                weather_stations = WeatherStation.objects.filter(wuid__in=reindex.split(","))
        else:
            weather_stations = WeatherStation.objects.exclude(wuresponse__isnull=True,wuresponse="")
        
        
        # Prepare progressbar
        #widgets = ['Updating weather:', Percentage(), ' ', Bar(), ' ', ETA()]
        #pbar = ProgressBar(widgets=widgets, maxval=weather_stations.count()).start()
        
        count = 1
        for weather_station in weather_stations:
            print "Updating weather for {0} ({1}/{2})".format(weather_station.wuid, count, weather_stations.count())
            weather_station.save()

            count += 1
#            pbar.update(count)
#            
#        pbar.finish()
