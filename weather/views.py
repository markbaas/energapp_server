# Create your views here.
from weather.models import WeatherStation, WuLimitExceeded, WeatherRequest
from django.http import HttpResponse
from django.contrib.gis.geos.geometry import GEOSGeometry
import datetime
from django.contrib.gis.geos.point import Point
from django.template.loader import get_template
from django.template.context import Context
from django.shortcuts import render_to_response
from django.contrib.gis.measure import D
import json
from dateutil import relativedelta
import time
import threading
from django.db.backends.util import logger


def get_closest_weather_station(request, coords):
    # lat,lon 
    lon, lat = [float(x) for x in coords.split(",")]
    
    # Get closest weather station
    point = Point(lon, lat)
    weather_station = WeatherStation.objects.distance(point).order_by('distance')[0] 
    distance = weather_station.distance
    weather_station = weather_station.to_dict()
    weather_station["distance"] = int(distance.km)
    
    return HttpResponse(json.dumps(weather_station), content_type="application/json")
 
def get_all_nearby_stations(request, coords, radius):
    # lat,lon 
    lon, lat = [float(x) for x in coords.split(",")]
    
    # Get closest weather station
    point = Point(lon, lat)
    weather_stations = WeatherStation.objects.filter(coordinates__distance_lt=(point, D(km=radius))).distance(point).order_by('distance')
    
    # If no result, just return the closest!
    if not weather_stations:
        return get_closest_weather_station(request, coords)

    # Loop and return data
    ws = []
    for w in weather_stations:
        data = w.to_dict()
        data["distance"] = int(w.distance.km)
        ws.append(data)
    
    return HttpResponse(json.dumps(ws), content_type="application/json")      

def get_conditions_result(request, request_id):
    wr = WeatherRequest.objects.get(id=request_id)
    
    response = json.dumps({"result": wr.result, 
                           "progress": wr.progress,
                           "sleep": wr.sleep,
                           })
    
    
    if wr.result: wr.delete() # Clean up
    return HttpResponse(response)
    
    

def get_conditions_by_weather_station(request, wuid, begin, end=None):
    #coords = "52.0295129,5.5324171"
    
    def get_conditions(wuid, begin, end, wr_id):
        weather_station = WeatherStation.objects.get(wuid=wuid)
        
        # Parse dates and create range
        begin = datetime.datetime.strptime(begin, "%Y-%m-%d")
        end = datetime.datetime.strptime(end, "%Y-%m-%d")
        days = abs((end-begin).days)+1 # add one as we want to include the end date
        begin = min(begin, end)
        conditions = []
        wr = WeatherRequest.objects.get(id=wr_id)
        for i in range(days):
            date = begin + relativedelta.relativedelta(days=i)
            logger.debug("getting weather for {0:%Y%m%d}".format(date))
            condition = None
            while not condition:
                try:
                    condition = weather_station.get_condition(date)
                    wr.progress = float(i)/days
                    wr.sleep = 10
                    wr.save()
                    #time.sleep(2)
                except WuLimitExceeded, err:
                    logger.debug("{0} sleeping for {1}".format(unicode(err), err.eta))
                    wr.progress = float(i)/days
                    wr.sleep = err.eta + 5 # margin of error of 5 seconds
                    wr.save()
                    time.sleep(err.eta)
                
            conditions.append(condition.to_dict())
        wr.result = json.dumps(conditions)
        wr.save()
    
    wr = WeatherRequest() 
    wr.progress = 0   
    wr.save()
    
    t = threading.Thread(target=get_conditions, args=(wuid, begin, end, wr.id))
    t.start()
    
    return HttpResponse(json.dumps({"wr_id": wr.id}), content_type="application/json")

def index(request):
    return render_to_response('index.html', {})